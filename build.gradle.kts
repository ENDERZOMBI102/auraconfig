@file:Suppress("UnstableApiUsage")
plugins {
	id( "org.jetbrains.kotlin.jvm" ) version "1.8.0"
	id( "xyz.wagyourtail.unimined" ) version "1.3.0-SNAPSHOT"
	`maven-publish`
}

repositories {
	mavenCentral()
	maven( "https://maven.shedaniel.me" )
	maven( "https://maven.fabricmc.net/" )
	maven( "https://maven.gegy.dev/releases/" )
	maven( "https://repsy.io/mvn/enderzombi102/mc" )
	maven( "https://maven.terraformersmc.com/releases" )
}

configurations {
	val implementation = this["implementation"]
	create( "fabric" ) { extendsFrom( implementation ) }
	create( "neoforge" ) { extendsFrom( implementation ) }
}
sourceSets {
	create( "fabric" ) { this.runtimeClasspath += sourceSets["test"].output }
	create( "neoforge" ) { this.runtimeClasspath += sourceSets["test"].output }
}

unimined {
	val configurer: xyz.wagyourtail.unimined.api.runs.RunConfig.() -> Unit = {
		workingDir = rootProject.file( "run" )
		jvmArgs( "-Dlog4j2.configurationFile=${rootProject.file( "log4j2.xml" ).absolutePath}" )
		jvmArgs( "-Dloader.disable_forked_guis=true" )
	}

	minecraft( sourceSets["main"], sourceSets["test"] ) {
		version( "1.20.2" )

		mappings {
			intermediary()
			yarn( 4 )

			devFallbackNamespace( "intermediary" )
		}
	}

	minecraft( sourceSets["fabric"] ) {
		combineWith( sourceSets["main"] )

		mappings {
			intermediary()
			yarn( 4 )

			devFallbackNamespace( "intermediary" )
		}

		runs {
			config( "client", configurer )
			config( "server", configurer )
		}

		fabric {
			loader( "0.15.1" )
		}
	}

	minecraft( sourceSets["neoforge"] ) {
		combineWith( sourceSets["main"] )

		mappings {
			intermediary()
			mojmap()
			mapping( "dev.lambdaurora:yalmm:1.21+build.4" ) {
				outputs( "yalmm", true ) { listOf( "intermediary" ) }
				mapNamespace( "named", "yalmm" )
				sourceNamespace( "intermediary" )
			}

			devFallbackNamespace( "intermediary" )
		}
		minecraftRemapper.config { ignoreConflicts(true) }

		runs {
			config( "client", configurer )
			config( "server", configurer )
		}

		neoForge {
			loader( "88" )
		}
	}
}

dependencies {
	implementation( "org.jetbrains:annotations:23.0.0" )
	implementation( "blue.endless:jankson:1.2.1" )
	implementation( "com.enderzombi102:EnderLib:0.3.3" )
	implementation( "com.squareup:javapoet:1.13.0" )

	"fabricModImplementation"( fabricApi.fabric( "0.77.0+1.19.2" ) )
	"fabricModImplementation"( "com.terraformersmc:modmenu:4.2.0-beta.2" )

	testAnnotationProcessor( sourceSets["main"].output )
	testAnnotationProcessor( sourceSets["main"].runtimeClasspath )
    testImplementation( platform( "org.junit:junit-bom:5.9.1" ) )
    testImplementation( "org.junit.jupiter:junit-jupiter" )
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<ProcessResources> {
	val props = mapOf(
		"version" to project.version,
		"minecraft_version" to "1.20.2",
		"description" to project.description
	)

	inputs.properties( props )
	filteringCharset = "UTF-8"

	filesMatching( "fabric.mod.json" ) {
		expand( props )
	}
	filesMatching( "mods.toml" ) {
		expand( props )
	}
}

tasks.withType<JavaCompile> {
	options.encoding = "UTF-8"
	options.release.set( 17 )
}

//java.toolchain.languageVersion.set( JavaLanguageVersion.of( 17 ) )

tasks.withType<Jar> {
	filesMatching( "LICENSE" ) {
		rename { "${it}_$archiveBaseName"}
	}
}

publishing {
	publications {
		create<MavenPublication>( "mavenJava" ) {
			from( components["java"] )
		}
	}

	repositories {
		mavenLocal()
		maven {
			name = "Repsy"
			credentials( PasswordCredentials::class )
			url = uri( "https://repsy.io/mvn/enderzombi102/mc" )
		}
	}
}
