package com.enderzombi102.enderlib.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;


@SuppressWarnings( "StringConcatenationArgumentToLogCall" )
public class NamespacedLogger implements Logger {
	private final String ns;
	private final Logger backing;

	public NamespacedLogger( String namespace ) {
		this( "[" + namespace + "] ", LoggerFactory.getLogger( namespace ) );
	}

	public NamespacedLogger( String namespace, Logger backing ) {
		this.ns = namespace;
		this.backing = backing;
	}

	@Override
	public String getName() {
		return backing.getName();
	}

	@Override
	public boolean isTraceEnabled() {
		return backing.isTraceEnabled();
	}

	@Override
	public void trace( String s ) {
		backing.trace( ns + s );
	}

	@Override
	public void trace( String s, Object o ) {
		backing.trace( ns + s, o );
	}

	@Override
	public void trace( String s, Object o, Object o1 ) {
		backing.trace( ns + s, o, o1 );
	}

	@Override
	public void trace( String s, Object... objects ) {
		backing.trace( ns + s, objects );
	}

	@Override
	public void trace( String s, Throwable throwable ) {
		backing.trace( ns + s, throwable );
	}

	@Override
	public boolean isTraceEnabled( Marker marker ) {
		return backing.isTraceEnabled();
	}

	@Override
	public void trace( Marker marker, String s ) {
		backing.trace( marker, s );
	}

	@Override
	public void trace( Marker marker, String s, Object o ) {
		backing.trace( marker, s, o );
	}

	@Override
	public void trace( Marker marker, String s, Object o, Object o1 ) {
		backing.trace( marker, s, o, o1 );
	}

	@Override
	public void trace( Marker marker, String s, Object... objects ) {
		backing.trace( marker, s, objects );
	}

	@Override
	public void trace( Marker marker, String s, Throwable throwable ) {
		backing.trace( marker, s, throwable );
	}

	@Override
	public boolean isDebugEnabled() {
		return backing.isDebugEnabled();
	}

	@Override
	public void debug( String s ) {
		backing.debug( ns + s );
	}

	@Override
	public void debug( String s, Object o ) {
		backing.debug( ns + s, o );
	}

	@Override
	public void debug( String s, Object o, Object o1 ) {
		backing.debug( ns + s, o, o1 );
	}

	@Override
	public void debug( String s, Object... objects ) {
		backing.debug( ns + s, objects );
	}

	@Override
	public void debug( String s, Throwable throwable ) {
		backing.debug( ns + s, throwable );
	}

	@Override
	public boolean isDebugEnabled( Marker marker ) {
		return backing.isDebugEnabled();
	}

	@Override
	public void debug( Marker marker, String s ) {
		backing.debug( marker, s );
	}

	@Override
	public void debug( Marker marker, String s, Object o ) {
		backing.debug( marker, s, o );
	}

	@Override
	public void debug( Marker marker, String s, Object o, Object o1 ) {
		backing.trace( marker, s, o, o1 );
	}

	@Override
	public void debug( Marker marker, String s, Object... objects ) {
		backing.trace( marker, s, objects );
	}

	@Override
	public void debug( Marker marker, String s, Throwable throwable ) {
		backing.trace( marker, s, throwable );
	}

	@Override
	public boolean isInfoEnabled() {
		return backing.isInfoEnabled();
	}

	@Override
	public void info( String s ) {
		backing.info( ns + s );
	}

	@Override
	public void info( String s, Object o ) {
		backing.info( ns + s, o );
	}

	@Override
	public void info( String s, Object o, Object o1 ) {
		backing.info( ns + s, o, o1 );
	}

	@Override
	public void info( String s, Object... objects ) {
		backing.info( ns + s, objects );
	}

	@Override
	public void info( String s, Throwable throwable ) {
		backing.info( ns + s, throwable );
	}

	@Override
	public boolean isInfoEnabled( Marker marker ) {
		return backing.isInfoEnabled();
	}

	@Override
	public void info( Marker marker, String s ) {
		backing.info( marker, s );
	}

	@Override
	public void info( Marker marker, String s, Object o ) {
		backing.info( marker, s, o );
	}

	@Override
	public void info( Marker marker, String s, Object o, Object o1 ) {
		backing.trace( marker, s, o, o1 );
	}

	@Override
	public void info( Marker marker, String s, Object... objects ) {
		backing.trace( marker, s, objects );
	}

	@Override
	public void info( Marker marker, String s, Throwable throwable ) {
		backing.trace( marker, s, throwable );
	}

	@Override
	public boolean isWarnEnabled() {
		return backing.isWarnEnabled();
	}

	@Override
	public void warn( String s ) {
		backing.warn( ns + s );
	}

	@Override
	public void warn( String s, Object o ) {
		backing.warn( ns + s, o );
	}

	@Override
	public void warn( String s, Object... objects ) {
		backing.warn( ns + s, objects );
	}

	@Override
	public void warn( String s, Object o, Object o1 ) {
		backing.warn( ns + s, o, o1 );
	}

	@Override
	public void warn( String s, Throwable throwable ) {
		backing.warn( ns + s, throwable );
	}

	@Override
	public boolean isWarnEnabled( Marker marker ) {
		return backing.isWarnEnabled();
	}

	@Override
	public void warn( Marker marker, String s ) {
		backing.warn( marker, s );
	}

	@Override
	public void warn( Marker marker, String s, Object o ) {
		backing.warn( marker, s, o );
	}

	@Override
	public void warn( Marker marker, String s, Object o, Object o1 ) {
		backing.trace( marker, s, o, o1 );
	}

	@Override
	public void warn( Marker marker, String s, Object... objects ) {
		backing.trace( marker, s, objects );
	}

	@Override
	public void warn( Marker marker, String s, Throwable throwable ) {
		backing.trace( marker, s, throwable );
	}

	@Override
	public boolean isErrorEnabled() {
		return backing.isErrorEnabled();
	}

	@Override
	public void error( String s ) {
		backing.error( ns + s );
	}

	@Override
	public void error( String s, Object o ) {
		backing.error( ns + s, o );
	}

	@Override
	public void error( String s, Object o, Object o1 ) {
		backing.error( ns + s, o, o1 );
	}

	@Override
	public void error( String s, Object... objects ) {
		backing.error( ns + s, objects );
	}

	@Override
	public void error( String s, Throwable throwable ) {
		backing.error( ns + s, throwable );
	}

	@Override
	public boolean isErrorEnabled( Marker marker ) {
		return backing.isErrorEnabled();
	}

	@Override
	public void error( Marker marker, String s ) {
		backing.error( marker, s );
	}

	@Override
	public void error( Marker marker, String s, Object o ) {
		backing.error( marker, s, o );
	}

	@Override
	public void error( Marker marker, String s, Object o, Object o1 ) {
		backing.trace( marker, s, o, o1 );
	}

	@Override
	public void error( Marker marker, String s, Object... objects ) {
		backing.trace( marker, s, objects );
	}

	@Override
	public void error( Marker marker, String s, Throwable throwable ) {
		backing.trace( marker, s, throwable );
	}
}
