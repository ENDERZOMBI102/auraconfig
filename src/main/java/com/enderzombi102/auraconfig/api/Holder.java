package com.enderzombi102.auraconfig.api;

import blue.endless.jankson.Jankson;
import blue.endless.jankson.JsonGrammar;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.command.ServerCommandSource;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;

public interface Holder<T> {
	@NotNull File getConfigFile();
	@NotNull String getModId();
	@NotNull T get();

	@NotNull Holder<T> withJankson( @NotNull Jankson jankson );
	@NotNull Holder<T> withGrammar( @NotNull JsonGrammar grammar );

	boolean hasModMenuCompat();
	@Nullable Screen getConfigScreen( @Nullable Screen parent );

	@Nullable LiteralArgumentBuilder<ServerCommandSource> getConfigCommand();

	boolean shouldSync();
	void writeToPacket( @NotNull PacketByteBuf buf );
	void readFromPacket( @NotNull PacketByteBuf buf );
}
