package com.enderzombi102.auraconfig.api.annotation;

import com.enderzombi102.auraconfig.api.DescriptorBuilder;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use these annotations to automatically generate a descriptor model for this Config entry.
 * This is the same as using the {@link DescriptorBuilder} to manually build the descriptor.
 */
@Target({ })
public @interface DescriptorModel {
	/**
	 * Adds a title to the field.
	 */
	@Retention( RetentionPolicy.CLASS )
	@Target( ElementType.FIELD )
	@interface Title {
		/**
		 * A literal line of text which will be used for the title, if empty, will use the translatable key {@code text.modid.config.$path.title}.
		 */
		String value() default "";

		/**
		 * Use the @{@link blue.endless.jankson.Comment}'s value for this tooltip.
		 */
		boolean comment() default false;
	}

	/**
	 * Adds a line of tooltips to the field.
	 */
	@Retention( RetentionPolicy.CLASS )
	@Target( ElementType.FIELD )
	@interface Tooltip {
		/**
		 * A literal line of text which will be used for the tooltip, if empty, will use the translatable key {@code text.modid.config.$path.tooltip$line}.
		 */
		String value() default "";

	}

	/**
	 *
	 */
	@Retention( RetentionPolicy.CLASS )
	@Target( ElementType.FIELD )
	@interface Subgroup {
		/**
		 * A literal line of text which will be used for the subgroup's title, if empty,
		 * will use the translatable key {@code text.modid.config.$path.tooltip$line}.
		 */
		String value() default "";
	}
}
