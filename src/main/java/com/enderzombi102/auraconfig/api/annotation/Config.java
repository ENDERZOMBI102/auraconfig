package com.enderzombi102.auraconfig.api.annotation;

import org.jetbrains.annotations.NotNull;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target( ElementType.TYPE )
@Retention( RetentionPolicy.SOURCE )
public @interface Config {
	/**
	 * The id of the mod registering the config.
	 */
	@NotNull String modid();

	/**
	 * The name of the config file.
	 */
	@NotNull String filename();

	/**
	 * Whether to generate a config screen for this config.
	 */
	boolean screen() default false;

	/**
	 * Whether to add the generated screen to the Mod Menu.
	 */
	boolean modmenu() default false;

	/**
	 * Whether to generate a brigadier node for this config.
	 */
	boolean brigadier() default false;
}
