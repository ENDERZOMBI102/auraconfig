package com.enderzombi102.auraconfig.api.annotation;

/**
 * Makes the annotated class/field sync between client and server.
 */
public @interface Sync {
	Mode value() default Mode.UseServer;

	enum Mode {
		/**
		 * Not synced, same as not adding the annotation
		 */
		None,
		/**
		 * Inform the server about the client's value
		 */
		TellServer,
		/**
		 * Use the server's value, implies `Sync.Mode.TellServer`
		 */
		UseServer
	}
}
