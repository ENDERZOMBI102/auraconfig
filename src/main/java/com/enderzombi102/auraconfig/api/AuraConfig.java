package com.enderzombi102.auraconfig.api;

import com.enderzombi102.auraconfig.impl.ConfigManager;
import net.minecraft.client.gui.screen.Screen;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class AuraConfig {
	public static <T> @NotNull Holder<T> register( @NotNull Class<T> configDataClass ) {
		return ConfigManager.get().createHolder( configDataClass );
	}

	public static @NotNull Optional<Screen> getConfigScreen( @NotNull String modId, @Nullable Screen parent ) {
		return ConfigManager.get()
			.getHolder( modId )
			.filter( Holder::hasModMenuCompat )
			.map( it -> it.getConfigScreen( parent ) );
	}
}
