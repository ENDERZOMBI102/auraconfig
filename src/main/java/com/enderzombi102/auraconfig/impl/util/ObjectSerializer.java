package com.enderzombi102.auraconfig.impl.util;

import com.enderzombi102.auraconfig.api.annotation.Sync;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

public class ObjectSerializer {
	private final @NotNull Map<Class<?>, ThrowingTriConsumer<NbtCompound, Field, Object> > acceptors = new HashMap<>();

	public ObjectSerializer() {
		this.acceptors.put( byte.class,       (nbt, field, obj) -> nbt.putInt(     field.getName(), field.getInt( obj ) ) );
		this.acceptors.put( short.class,      (nbt, field, obj) -> nbt.putShort(   field.getName(), field.getShort( obj ) ) );
		this.acceptors.put( int.class,        (nbt, field, obj) -> nbt.putInt(     field.getName(), field.getInt( obj ) ) );
		this.acceptors.put( long.class,       (nbt, field, obj) -> nbt.putLong(    field.getName(), field.getLong( obj ) ) );
		this.acceptors.put( float.class,      (nbt, field, obj) -> nbt.putFloat(   field.getName(), field.getFloat( obj ) ) );
		this.acceptors.put( double.class,     (nbt, field, obj) -> nbt.putDouble(  field.getName(), field.getDouble( obj ) ) );
		this.acceptors.put( boolean.class,    (nbt, field, obj) -> nbt.putBoolean( field.getName(), field.getBoolean( obj ) ) );
		this.acceptors.put( String.class,     (nbt, field, obj) -> nbt.putString(  field.getName(), (String) field.get( obj ) ) );
		this.acceptors.put( BlockPos.class,   (nbt, field, obj) -> nbt.putLong(    field.getName(), ( (BlockPos) field.get( obj ) ).asLong() ) );
		this.acceptors.put( Identifier.class, (nbt, field, obj) -> nbt.putString(  field.getName(), field.get( obj ).toString() ) );
	}

	public @NotNull NbtCompound visitObject( @NotNull Object obj, boolean shouldSync ) {
		try {
			var nbt = new NbtCompound();
			for ( var field : obj.getClass().getDeclaredFields() ) {
				var ann = field.getDeclaredAnnotation( Sync.class );
				if (! Modifier.isPublic( field.getModifiers() ) )
					continue;

				if ( ann == null || ann.value() != Sync.Mode.UseServer || !shouldSync )
					continue;

				if ( acceptors.containsKey( field.getType() ) )
					this.acceptors.get( field.getType() ).consume( nbt, field, obj );
				else;

			}
			return nbt;
		} catch ( IllegalAccessException e ) {
			throw new RuntimeException( e );
		}
	}
}
