package com.enderzombi102.auraconfig.impl.util;

@FunctionalInterface
public interface ThrowingTriConsumer<X, Y, Z> {
	void consume( X x, Y y, Z z ) throws IllegalAccessException;
}
