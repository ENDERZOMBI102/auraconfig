package com.enderzombi102.auraconfig.impl.util;

import net.minecraft.util.Identifier;
import org.jetbrains.annotations.NotNull;

public class Const {
	public static final @NotNull String ID = "aura-config";
	public static final @NotNull Identifier CONFIG_SYNC_ID = getId( "config_sync" );

	public static @NotNull Identifier getId( @NotNull String path ) {
		return new Identifier( ID, path );
	}
}
