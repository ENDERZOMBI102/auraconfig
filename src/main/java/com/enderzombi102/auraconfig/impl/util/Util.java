package com.enderzombi102.auraconfig.impl.util;

import blue.endless.jankson.Jankson;
import blue.endless.jankson.JsonGrammar;
import net.minecraft.text.Text;
import org.jetbrains.annotations.NotNull;

public class Util {
	public static final JsonGrammar DEFAULT_GRAMMAR = JsonGrammar.JANKSON;
	public static final Jankson DEFAULT_JANKSON = Jankson.builder().build();

	public static @NotNull Text translate( @NotNull String modId, @NotNull String path ) {
		return Text.translatable( "text.config.%s.%s".formatted( modId, path ) );
	}
}
