package com.enderzombi102.auraconfig.impl;

import com.enderzombi102.auraconfig.impl.gen.ConfigRuntimeMetadata;
import com.enderzombi102.auraconfig.impl.platform.LoaderPlatform;
import com.enderzombi102.enderlib.logging.NamespacedLogger;
import net.minecraft.server.MinecraftServer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

import java.util.ServiceLoader;

public class AuraConfig {
	@SuppressWarnings( "rawtypes" )
	public static final @NotNull ServiceLoader<ConfigRuntimeMetadata> SERVICE_LOADER = ServiceLoader.load( ConfigRuntimeMetadata.class, AuraConfig.class.getClassLoader() );
	public static final @NotNull Logger LOGGER = new NamespacedLogger( "AuraConfig" );
	public static @Nullable MinecraftServer server = null;

	private static @Nullable LoaderPlatform loaderPlatform = null;

	public static @NotNull LoaderPlatform loaderPlatform() {
		if ( loaderPlatform == null )
			loaderPlatform = ServiceLoader.load( LoaderPlatform.class )
				.stream()
				.map( ServiceLoader.Provider::get )
				.filter( LoaderPlatform::isUsable )
				.findFirst()
				.orElseThrow();

		return loaderPlatform;
	}
}
