package com.enderzombi102.auraconfig.impl.platform;

import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;

import java.nio.file.Path;

public interface LoaderPlatform {
	boolean isUsable();

	String getModVersion( String modId );

	Path getConfigDir();

	void serverSendPacket( ServerPlayerEntity player, Identifier channel, PacketByteBuf buf );
}
