package com.enderzombi102.auraconfig.impl.processor;

import com.enderzombi102.auraconfig.api.annotation.Config;
import com.squareup.javapoet.*;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.processing.Completion;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;
import javax.tools.Diagnostic;
import javax.tools.StandardLocation;
import java.io.IOException;
import java.util.*;

public class AuraConfigProcessor implements Processor {
	private static final @NotNull String GEN_CLASS_PKG = "com.enderzombi102.auraconfig.impl.gen";
	private static final ClassName METADATA_CLASS = ClassName.get( GEN_CLASS_PKG, "ConfigRuntimeMetadata" );
	private static final ClassName NBT_COMPOUND = ClassName.get( "net.minecraft.nbt", "NbtCompound" );
	private static final ClassName NOT_NULL = ClassName.get( "org.jetbrains.annotations", "NotNull" );

	private final @NotNull List<AnnotationData> configDataTypes = new ArrayList<>();
	private ProcessingEnvironment env;
	private boolean initialized = false;
	private boolean processed = false;


	@Override
	public void init( @NotNull ProcessingEnvironment env ) {
		if ( this.initialized )
			throw new IllegalStateException("Cannot call init more than once.");
		Objects.requireNonNull( env, "Tool provided null ProcessingEnvironment" );

		this.initialized = true;
		this.env = env;
	}

	@Override
	public @NotNull Set<String> getSupportedOptions() {
		return Set.of();
	}

	@Override
	public @NotNull Set<String> getSupportedAnnotationTypes() {
		return Set.of( "com.enderzombi102.auraconfig.api.annotation.*" );
	}

	@Override
	public @NotNull SourceVersion getSupportedSourceVersion() {
		return switch ( Runtime.version().feature() ) {
			case 17 -> SourceVersion.RELEASE_17;
			case 16 -> SourceVersion.RELEASE_16;
			default -> SourceVersion.RELEASE_8;
		};
	}

	@Override
	public boolean process( @NotNull Set<? extends TypeElement> annotations, @NotNull RoundEnvironment roundEnv ) {
		if ( processed )
			return false;

		for ( var ann : annotations ) {
			if ( ann.getSimpleName().toString().equals( "Config" ) )
				this.processConfig( roundEnv );
		}
		try {
			this.generateRuntimeHelper();
		} catch ( IOException e ) {
			throw new RuntimeException( e );
		}

		this.processed = true;
		return true;
	}

	@Override
	@Contract("null,_,_,null->fail;null,_,_,_->_;_,_,_,null->_")
	public @NotNull Iterable<? extends Completion> getCompletions( @Nullable Element element, @Nullable AnnotationMirror annotation, @Nullable ExecutableElement member, @Nullable String userText ) {
		if ( element == null && userText == null )
			throw new IllegalArgumentException( "At least one of `element` and `userText` must not be `null`!" );

		return Collections.emptyList();
	}

	private void processConfig( @NotNull RoundEnvironment roundEnv ) {
		for ( var type : roundEnv.getElementsAnnotatedWith( Config.class ) ) {
			var ann = type.getAnnotation( Config.class );

			var constructor = type.getEnclosedElements()
				.stream()
				.filter( it -> it.getKind() == ElementKind.CONSTRUCTOR )
				.filter( it -> it.getModifiers().contains( Modifier.PUBLIC ) )
				.filter( it -> it.asType().toString().equals( "()void" ) )
				.findFirst();

			if ( constructor.isEmpty() ) {
				this.env.getMessager().printMessage( Diagnostic.Kind.ERROR, "No valid constructor found, please declare a public parameterless constructor" );
				continue;
			}

			this.configDataTypes.add( AnnotationData.create( ann, type ) );
		}
	}

	private void generateRuntimeHelper() throws IOException {
		var elements = this.env.getElementUtils();
		var types = this.env.getTypeUtils();

		for ( var configEntry : this.configDataTypes ) {
			var runtimeDataFile = this.env.getFiler()
				.createSourceFile( GEN_CLASS_PKG + "." + configEntry.serviceClassName(), configEntry.typeElement() );
			runtimeDataFile.delete();

			var configDataType = configEntry.typeElement().asType();
			var type = TypeSpec.classBuilder( configEntry.serviceClassName() )
				.superclass( ParameterizedTypeName.get( METADATA_CLASS, ClassName.get( configDataType ) ) )
				.addInitializerBlock( CodeBlock.builder()
					.addStatement( "this.modId = $S", configEntry.modId() )
					.addStatement( "this.filename = $S", configEntry.filename() )
					.addStatement( "this.clazz = $T.class", configDataType )
					.addStatement( "this.constructor = $T::new", configDataType )
					.build()
				)
				.addMethod( MethodSpec.methodBuilder( "toNbt" )
					.addAnnotation( Override.class )
					.addModifiers( Modifier.PUBLIC )
					.addParameter( ParameterSpec.builder( ClassName.get( configDataType ), "obj" )
						.addAnnotation( NOT_NULL )
						.build()
					)
					.returns( notNull( NBT_COMPOUND ) )
					.addCode( this.buildNbtSerializer( configEntry ) )
					.build()
				)
				.addMethod( MethodSpec.methodBuilder( "fromNbt" )
					.addAnnotation( Override.class )
					.addModifiers( Modifier.PUBLIC )
					.addParameter( ParameterSpec.builder( ClassName.get( configDataType ), "obj" )
						.addAnnotation( NOT_NULL )
						.build()
					)
					.addParameter( ParameterSpec.builder( NBT_COMPOUND, "nbt" )
						.addAnnotation( NOT_NULL )
						.build()
					)
					.returns( notNull( ClassName.get( configDataType ) ) )
				.addCode( this.buildNbtDeserializer( configEntry ) )
					.build()
				)
				.build();

			try ( var writer = runtimeDataFile.openWriter() ) {
				writer.write( type.toString() );
			}
		}

		var serviceFile = this.env.getFiler().createResource(
			StandardLocation.CLASS_OUTPUT,
			"",
			"META-INF/services/" + GEN_CLASS_PKG + ".ConfigRuntimeMetadata"
		);
		serviceFile.delete();

		try ( var writer = serviceFile.openWriter() ) {
			for ( var configEntry : this.configDataTypes )
				writer.append( GEN_CLASS_PKG ).append( "." ).append( configEntry.serviceClassName() ).append( "\n" );
		}
	}

	private @NotNull CodeBlock buildNbtDeserializer( @NotNull AnnotationData configEntry ) {
		return CodeBlock.builder()
			.addStatement( "return null" )
			.build();
	}

	private @NotNull CodeBlock buildNbtSerializer( @NotNull AnnotationData configEntry ) {
		return CodeBlock.builder()
			.addStatement( "return null" )
			.build();
	}

	private @NotNull TypeName notNull( TypeName og ) {
		return og.annotated( AnnotationSpec.builder( NOT_NULL ).build() );
	}
}
