package com.enderzombi102.auraconfig.impl.processor;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.lang.model.element.Modifier;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ClassBuilder {
	private @NotNull String pkg = "";
	private @NotNull String name = "";
	private @Nullable String extended = null;
	private final @NotNull Set<String> imports = new HashSet<>();
	private final @NotNull Set<Modifier> modifiers = new HashSet<>();
	private final @NotNull Set<String> implemented = new HashSet<>();
	private final @NotNull List<Generic> generics = new ArrayList<>();
	private final @NotNull List<Block> initializers = new ArrayList<>();

	public @NotNull ClassBuilder withPackage( @NotNull String pkg ) {
		this.pkg = pkg;
		return this;
	}

	public @NotNull ClassBuilder withImport( @NotNull String imp ) {
		this.imports.add( imp );
		return this;
	}

	public @NotNull ClassBuilder withGeneric( @NotNull String name, @NotNull String constrain ) {
		if ( this.generics.stream().anyMatch( it -> it.name().equals( name ) ) )
			throw new IllegalArgumentException( "There already is a generic parameter named `%s`!".formatted( name ) );

		this.generics.add( new Generic( name, constrain ) );
		return this;
	}

	public @NotNull ClassBuilder withGeneric( @NotNull String name ) {
		if ( this.generics.stream().anyMatch( it -> it.name().equals( name ) ) )
			throw new IllegalArgumentException( "There already is a generic parameter named `%s`!".formatted( name ) );

		this.generics.add( new Generic( name, null ) );
		return this;
	}

	public @NotNull ClassBuilder withModifier( @NotNull Modifier modifier ) {
		this.modifiers.add( modifier );
		return this;
	}

//	public @NotNull ClassBuilder withInitializer( @NotNull BlockBuilder modifier ) {
//		this.modifiers.add( modifier );
//		return this;
//	}

	public void writeTo( @NotNull Writer writer ) throws IOException {
		// package
		writer.append( "package " ).append( this.pkg ).append( ";\n" );
		writer.append( "\n" );

		// imports
		for ( var imp : this.imports )
			writer.append( "import " ).append( imp ).append( ";" );
		writer.append( "\n\n" );

		// class modifiers
		for ( var mod : this.modifiers )
			writer.append( mod.toString() ).append( " " );

		// `class` kw + name
		writer.append( "class " ).append( this.name );

		// `extends` clause
		if ( this.extended != null )
			writer.append( " extends " ).append( this.extended );

		// `implements` clause
		if (! this.implemented.isEmpty() ) {
			writer.append( " implements" );
			var multiple = false;

			for ( var iface : this.implemented ) {
				if ( multiple )
					writer.append( "," );

				writer.append( " " ).append( iface );
				multiple = true;
			}
		}

		writer.append( " {\n" );

//		if (! this.initializers.isEmpty() )
//			for ( var init : this.initializers ) {
//				if ( multiple )
//					writer.append( "," );
//
//				writer.append( " " ).append( iface );
//				multiple = true;
//			}
//
//		writer.append( "\tthis.modId = \"" ).append( configEntry.modId ).append( "\";\n" );
//		writer.append( "\tthis.filename = \"" ).append( configEntry.filename ).append( "\";\n" );
//		writer.append( "\tthis.configClass = " ).append( configEntry.configClassName ).append( ".class;\n" );
//		writer.append( "}}\n" );
	}

	@Override
	public String toString() {
		var writer = new StringWriter();
		try {
			this.writeTo( writer );
		} catch ( IOException e ) {
			throw new RuntimeException( e );
		}
		return writer.toString();
	}

	private record Generic( @NotNull String name, @Nullable String constrain ) { }
	private record Block( @NotNull String name, @Nullable String constrain ) { }
}
