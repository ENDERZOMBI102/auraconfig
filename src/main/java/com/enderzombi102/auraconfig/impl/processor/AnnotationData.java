package com.enderzombi102.auraconfig.impl.processor;


import com.enderzombi102.auraconfig.api.annotation.Config;
import org.jetbrains.annotations.NotNull;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.VariableElement;
import java.util.List;

public record AnnotationData(
	boolean modmenu,
	@NotNull String modId,
	@NotNull String filename,
	@NotNull String configClassName,
	@NotNull String serviceClassName,
	@NotNull List<VariableElement> fields,
	@NotNull Element typeElement
) {
	public static @NotNull AnnotationData create( @NotNull Config ann, @NotNull Element type ) {
		var modIdBuilder = new StringBuilder()
			.append( Character.toUpperCase( ann.modid().charAt( 0 ) ) );

		{
			var splitted = false;
			for ( var i = 0; i < ann.modid().length(); i += 1 ) {
				var chat = ann.modid().charAt( i );
				if ( chat == '-' ) {
					splitted = true;
				} else if ( splitted ) {
					modIdBuilder.append( Character.toUpperCase( chat ) );
					splitted = false;
				} else {
					modIdBuilder.append( chat );
				}
			}
		}

		var fields = type.getEnclosedElements()
			.stream()
			.filter( it -> it.getKind() == ElementKind.FIELD )
			.map( VariableElement.class::cast )
			.toList();

		return new AnnotationData(
			ann.modmenu(),
			ann.modid(),
			ann.filename(),
			type.getSimpleName().toString(),
			type.getSimpleName().toString() + "$" + modIdBuilder,
			fields,
			type
		);
	}
}
