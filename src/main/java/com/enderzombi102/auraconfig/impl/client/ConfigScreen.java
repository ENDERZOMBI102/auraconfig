package com.enderzombi102.auraconfig.impl.client;

import com.enderzombi102.auraconfig.impl.HolderImpl;
import net.minecraft.client.gui.screen.Screen;
import org.jetbrains.annotations.NotNull;

import static com.enderzombi102.auraconfig.impl.util.Util.translate;

public class ConfigScreen extends Screen {
	public ConfigScreen( @NotNull HolderImpl<?> holder ) {
		super( translate( holder.getModId(), "title" ) );


	}
}
