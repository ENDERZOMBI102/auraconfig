package com.enderzombi102.auraconfig.impl;

import blue.endless.jankson.Jankson;
import blue.endless.jankson.JsonGrammar;
import blue.endless.jankson.api.DeserializationException;
import blue.endless.jankson.api.SyntaxError;
import com.enderzombi102.auraconfig.api.Holder;
import com.enderzombi102.auraconfig.impl.gen.ConfigRuntimeMetadata;
import com.enderzombi102.auraconfig.impl.util.Util;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.TitleScreen;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.command.ServerCommandSource;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.function.Supplier;

import static com.mojang.text2speech.Narrator.LOGGER;

public class HolderImpl<T> implements Holder<T> {
	private final @Nullable Supplier<Screen> screenFactory;
	private final @NotNull ConfigRuntimeMetadata<T> metadata;
	private final @NotNull String modVersion;
	private final @NotNull File configFile;
	private final boolean shouldSync;
	private @NotNull JsonGrammar grammar = Util.DEFAULT_GRAMMAR;
	private @NotNull Jankson jankson = Util.DEFAULT_JANKSON;
	private @NotNull State state = State.None;
	private @Nullable T configData = null;

	@SuppressWarnings( "unchecked" )
	public HolderImpl( @NotNull Class<T> configDataClass ) {
		var metadata = AuraConfig.SERVICE_LOADER
			.stream()
			.filter( it -> it.type() == configDataClass )
			.findFirst()
			.orElseThrow( () -> new IllegalStateException( "Config class metadata `%s` was not compiled correctly!".formatted( configDataClass ) ) )
			.get();

		this.metadata = metadata;

		this.modVersion = AuraConfig.loaderPlatform()
			.getModVersion( this.metadata.modId );

		this.configFile = AuraConfig.loaderPlatform()
			.getConfigDir()
			.resolve( metadata.filename )
			.toFile();

		this.screenFactory = TitleScreen::new;
		this.shouldSync = false;
	}

	@Override
	public @NotNull T get() {
		if ( this.configData == null )
			this.loadFromDisk();

		return this.configData;
	}

	@Override
	public @NotNull Holder<T> withJankson( @NotNull Jankson jankson ) {
		this.jankson = jankson;
		return this;
	}

	@Override
	public @NotNull Holder<T> withGrammar( @NotNull JsonGrammar grammar ) {
		this.grammar = grammar;
		return this;
	}

	@Override
	public @NotNull File getConfigFile() {
		return this.configFile;
	}

	@Override
	public @NotNull String getModId() {
		return this.metadata.modId;
	}

	@Override
	public boolean hasModMenuCompat() {
		return this.screenFactory != null;
	}

	@Override
	public @Nullable Screen getConfigScreen( @Nullable Screen screen ) {
		return this.screenFactory != null ? this.screenFactory.get() : null;
	}

	@Override
	public @Nullable LiteralArgumentBuilder<ServerCommandSource> getConfigCommand() {
		return null;
	}

	@Override
	public boolean shouldSync() {
		return this.shouldSync;
	}

	@Override
	public void writeToPacket( @NotNull PacketByteBuf buf ) {
		buf.writeString( this.metadata.modId );
		buf.writeString( this.modVersion );
		buf.writeNbt( this.toNbt() );
	}

	@Override
	public void readFromPacket( @NotNull PacketByteBuf buf ) {
		var modVersion = buf.readString();
		if (! this.modVersion.equals( modVersion ) )
			throw new IllegalStateException( "Received config for mod `%s`, but it has a different version attached, `%s` (server) vs `%s` (local)".formatted( this.metadata.modId, modVersion, this.modVersion ) );

		var nbt = buf.readNbt();
		assert nbt != null : "Received invalid config for mod `%s`, it was `null`!!".formatted( this.metadata.modId );
		MinecraftClient.getInstance().execute( () -> this.loadFromNbt( nbt ) );
	}

	private void loadFromDisk() {
		LOGGER.debug( "Loading `{}`'s config...", this.metadata.modId );

		if (! this.configFile.exists() ) {
			LOGGER.debug( "Mod `{}` does not have a config file on disk, creating...", this.metadata.modId );
			this.configData = this.metadata.constructor.get();
			this.saveToDisk();
			return;
		}

		try {
			this.configData = this.jankson.fromJsonCarefully( jankson.load( this.configFile ), this.metadata.clazz );
			this.state = State.Disk;

			LOGGER.info( "`{}`'s config loaded", this.metadata.modId );
		} catch ( DeserializationException | SyntaxError | IOException e ) {
			LOGGER.error( "Failed to load `{}`'s config: ", this.metadata.modId, e );
		}
	}

	private void saveToDisk() {
		if ( state != State.Disk )
			return;

		try {
			LOGGER.debug( "Saving `{}`'s config to disk...", this.metadata.modId );
			Files.writeString(
				this.configFile.toPath(),
				this.jankson.toJson( this.configData )
					.toJson( this.grammar )
			);

			ConfigManager.get().broadcastConfigChange( this );
		} catch ( IOException e ) {
			LOGGER.error( "Failed to save `{}`'s config to disk: ", this.metadata.modId, e );
		}
	}

	private @NotNull NbtCompound toNbt() {
		assert this.configData != null : "Tried to serialize without a config instance!";
		return this.metadata.toNbt( this.configData );
	}

	private void loadFromNbt( @NotNull NbtCompound nbt ) {
		var current = this.configData;
		if ( current == null )
			current = this.metadata.constructor.get();

		this.configData = this.metadata.fromNbt( current, nbt );
	}

	private enum State {
		None,
		Disk,
		Detached
	}
}
