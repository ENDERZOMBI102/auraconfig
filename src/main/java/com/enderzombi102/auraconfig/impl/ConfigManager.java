package com.enderzombi102.auraconfig.impl;

import com.enderzombi102.auraconfig.api.Holder;
import io.netty.buffer.Unpooled;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.PacketByteBuf;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.enderzombi102.auraconfig.impl.AuraConfig.LOGGER;
import static com.enderzombi102.auraconfig.impl.util.Const.CONFIG_SYNC_ID;


public class ConfigManager {
	private static final @NotNull ConfigManager INSTANCE = new ConfigManager();
	private final @NotNull List<Holder<?>> holders = new ArrayList<>();

	public @NotNull Collection<@NotNull Holder<?>> getHolders() {
		return this.holders;
	}

	public <T> @NotNull Holder<T> createHolder( @NotNull Class<T> configDataClass ) {
		var holder = new HolderImpl<>( configDataClass );
		this.holders.add( holder );
		return holder;
	}


	public void sync( @NotNull String version, @Nullable NbtCompound config ) {
		// TODO: Sync configs to client, inform server about client's
	}

	public void broadcastConfigChange( @NotNull Holder<?> holder ) {
		var server = AuraConfig.server;
		if ( server == null )
			return;

		var buf = new PacketByteBuf( Unpooled.buffer() );
		holder.writeToPacket( buf );

		LOGGER.debug( "Sending config update for mod `{}` to clients", holder.getModId() );
		for ( var player : server.getPlayerManager().getPlayerList() )
			AuraConfig.loaderPlatform().serverSendPacket( player, CONFIG_SYNC_ID, buf );
	}

	public void resetConfigs() {
		// TODO: Reset configs to disk state
	}

	public Optional<Holder<?>> getHolder( String modId ) {
		return this.holders.stream()
			.filter( it -> it.getModId().equals( modId ) )
			.findFirst();
	}

	public static @NotNull ConfigManager get() {
		return INSTANCE;
	}
}
