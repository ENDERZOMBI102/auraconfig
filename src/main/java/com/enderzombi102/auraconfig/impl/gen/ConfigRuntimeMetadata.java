package com.enderzombi102.auraconfig.impl.gen;

import net.minecraft.nbt.NbtCompound;
import org.jetbrains.annotations.NotNull;

import java.util.function.Supplier;

@SuppressWarnings( "NotNullFieldNotInitialized" )
public abstract class ConfigRuntimeMetadata<T> {
	public @NotNull String modId;
	public @NotNull String filename;
	public @NotNull Class<T> clazz;
	public @NotNull Supplier<T> constructor;

	public abstract @NotNull NbtCompound toNbt( @NotNull T obj );
	public abstract @NotNull T fromNbt( @NotNull T obj, @NotNull NbtCompound nbt );
}
