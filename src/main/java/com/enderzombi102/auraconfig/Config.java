package com.enderzombi102.auraconfig;

public class Config {
//	public static void loadFromPacket( @NotNull String version, @Nullable NbtCompound nbt ) {
//		LOGGER.info( "[Elysium] Loading config received from server" );
//		assert nbt != null;
//
//		if (! version.equals( Const.VERSION ) )
//			throw new IllegalStateException( "Config received from server has a different mod version attached: %s (current) vs %s (received)".formatted( Const.VERSION, version ) );
//
//		var config = new ConfigData();
//
//		var logging = int2bool( nbt.getInt( "logging" ) );
//		config.logging.summonRandom = logging[ 0 ];
//		config.logging.claimAnchor = logging[ 1 ];
//		config.logging.claimAnchorScreen = logging[ 2 ];
//		config.logging.virtualResourcePack = logging[ 3 ];
//
//		var features = int2bool( nbt.getInt( "features" ) );
//		config.features.reloadOrigins = features[ 0 ];
//		config.features.claimModifications = features[ 1 ];
//		config.features.dreamingGomlFixes = features[ 2 ];
//
//		{
//			var spawn = nbt.getCompound( "spawn" );
//			if ( spawn.contains( "center" ) )
//				config.spawn.centerPosition = BlockPos.fromLong( spawn.getLong( "center" ) );
//			config.spawn.radius = spawn.getInt( "radius" );
//		}
//		{
//			var taxes = nbt.getCompound( "taxes" );
//			var longs = taxes.getLongArray( "long" );
//			config.taxes.taxMultiplier = longs[ 0 ];
//			config.taxes.taxZoneRange = longs[ 1 ];
//			config.taxes.minimumFee = longs[ 2 ];
//			config.taxes.maximumFee = longs[ 3 ];
//			var ints = taxes.getIntArray( "int" );
//			config.taxes.equation = TaxesData.EquationType.values()[ ints[ 0 ] ];
//			config.taxes.claimScreenInputCycleTime = ints[ 1 ];
//		}
//		{
//			config.summonRandomCategories.clear();
//			var summons = nbt.getCompound( "summons" );
//			for ( var key : summons.getKeys() )
//				config.summonRandomCategories.put(
//					key,
//					summons.getList( key, NbtElement.STRING_TYPE )
//						.stream()
//						.map( NbtElement::asString )
//						.map( Identifier::new )
//						.toList()
//				);
//		}
//
//		SOURCE = ConfigFrom.Server;
//		DATA = config;
//		LOGGER.info( "[Elysium] Loaded config from server" );
//	}
//
//	public static @NotNull PacketByteBuf getPacketBuf() {
//		var config = get();
//		var nbt = new NbtCompound();
//		nbt.putInt( "logging", bool2int(
//			config.logging.summonRandom,
//			config.logging.claimAnchor,
//			config.logging.claimAnchorScreen,
//			config.logging.virtualResourcePack
//		) );
//		nbt.putInt( "features", bool2int(
//			config.features.reloadOrigins,
//			config.features.claimModifications,
//			config.features.dreamingGomlFixes
//		) );
//		{
//			var tmp = new NbtCompound();
//			if ( config.spawn.centerPosition != null )
//				tmp.putLong( "center", config.spawn.centerPosition.asLong() );
//			tmp.putInt( "radius", config.spawn.radius );
//			nbt.put( "spawn", tmp );
//		}
//		{
//			var tmp = new NbtCompound();
//			tmp.putLongArray( "long", new long[] {
//				config.taxes.taxMultiplier,
//				config.taxes.taxZoneRange,
//				config.taxes.minimumFee,
//				config.taxes.maximumFee
//			});
//			tmp.putIntArray( "int", new int[] {
//				config.taxes.equation.ordinal(),
//				config.taxes.claimScreenInputCycleTime
//			});
//			nbt.put( "taxes", tmp );
//		}
//		{
//			nbt.put(
//				"summons",
//				config.summonRandomCategories.entrySet()
//					.stream()
//					.map(
//						entry -> Map.entry(
//							entry.getKey(),
//							entry.getValue()
//								.stream()
//								.map( Identifier::toString )
//								.map( NbtString::of )
//								.collect( NbtList::new, AbstractList::add, AbstractCollection::addAll )
//						)
//					)
//					.collect( NbtCompound::new, ( comp, entry ) -> comp.put( entry.getKey(), entry.getValue() ), NbtCompound::copyFrom )
//			);
//		}
//		return PacketByteBufs.create().writeString( Const.VERSION ).writeNbt( nbt );
//	}
}
