package com.enderzombi102.auraconfig.platform.fabric.mixin;

import com.enderzombi102.auraconfig.impl.ConfigManager;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.network.ClientConnection;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import static com.enderzombi102.auraconfig.impl.util.Const.CONFIG_SYNC_ID;

@Mixin( PlayerManager.class )
public class PlayerManagerMixin {
	@Inject( method = "onPlayerConnect", at = @At( "TAIL" ) )
	public void onPlayerConnect( ClientConnection connection, ServerPlayerEntity player, CallbackInfo ci ) {
		for ( var holder : ConfigManager.get().getHolders() )
			if ( holder.shouldSync() ) {
				var buf = PacketByteBufs.create();
				holder.writeToPacket( buf );
				ServerPlayNetworking.send( player, CONFIG_SYNC_ID, buf );
			}
	}
}
