package com.enderzombi102.auraconfig.platform.fabric;

import com.enderzombi102.auraconfig.impl.platform.LoaderPlatform;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;

import java.nio.file.Path;

public class FabricLoaderPlatform implements LoaderPlatform {
	@Override
	public boolean isUsable() {
		try {
			Class.forName( "net.fabricmc.loader.api.FabricLoader", false, null );
		} catch ( ClassNotFoundException e ) {
			return false;
		}
		return true;
	}

	@Override
	public String getModVersion( String modId ) {
		return FabricLoader.getInstance()
			.getModContainer( modId )
			.orElseThrow( () -> new IllegalStateException( "Mod `%s` tried to register a config while it doesn't exist??".formatted( modId ) ) )
			.getMetadata()
			.getVersion()
			.getFriendlyString();
	}

	@Override
	public Path getConfigDir() {
		return FabricLoader.getInstance().getConfigDir();
	}

	@Override
	public void serverSendPacket( ServerPlayerEntity player, Identifier channel, PacketByteBuf buf ) {
		ServerPlayNetworking.send( player, channel, buf );
	}
}
