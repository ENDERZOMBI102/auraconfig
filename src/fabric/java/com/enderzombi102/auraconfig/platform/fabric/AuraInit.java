package com.enderzombi102.auraconfig.platform.fabric;

import com.enderzombi102.auraconfig.impl.AuraConfig;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;

public class AuraInit implements ModInitializer {
	@Override
	public void onInitialize() {
		AuraConfig.LOGGER.info( "Initializing AuraConfig on fabric" );
		ServerLifecycleEvents.SERVER_STARTED.register( server -> AuraConfig.server = server );
	}
}
