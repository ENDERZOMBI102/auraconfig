package com.enderzombi102.auraconfig.platform.fabric.client;

import com.enderzombi102.auraconfig.impl.ConfigManager;
import com.enderzombi102.auraconfig.api.Holder;
import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;

import java.util.Map;
import java.util.stream.Collectors;

public class AuraInitModMenu implements ModMenuApi {
	@Override
	public Map< String, ConfigScreenFactory<?> > getProvidedConfigScreenFactories() {
		return ConfigManager.get()
			.getHolders()
			.stream()
			.filter( Holder::hasModMenuCompat )
			.collect( Collectors.toMap( Holder::getModId, it -> it::getConfigScreen ) );
	}
}
