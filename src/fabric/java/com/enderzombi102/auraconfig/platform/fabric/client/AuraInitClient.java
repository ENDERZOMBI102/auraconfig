package com.enderzombi102.auraconfig.platform.fabric.client;

import com.enderzombi102.auraconfig.impl.ConfigManager;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;

import static com.enderzombi102.auraconfig.impl.util.Const.CONFIG_SYNC_ID;


public class AuraInitClient implements ClientModInitializer {
	@Override
	public void onInitializeClient() {
		ClientPlayNetworking.registerGlobalReceiver( CONFIG_SYNC_ID, ( client, networkHandler, data, sender ) -> {
			var modId = data.readString();

			ConfigManager.get()
				.getHolder( modId )
				.orElseThrow()
				.readFromPacket( data );
		});
	}
}
