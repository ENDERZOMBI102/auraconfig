package testmod;

import net.minecraft.util.Identifier;
import org.jetbrains.annotations.NotNull;

public class Const {
	public static final @NotNull String ID = "auraconfig-test";

	public static @NotNull Identifier getId( @NotNull String path ) {
		return new Identifier( ID, path );
	}
}
