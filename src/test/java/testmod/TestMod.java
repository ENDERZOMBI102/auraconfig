package testmod;

import net.fabricmc.api.ModInitializer;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import testmod.config.Config;

@SuppressWarnings( "Deprecation" )
public class TestMod implements ModInitializer {
	private static final @NotNull Logger LOGGER = LoggerFactory.getLogger( "TestMod" );

	@Override
	public void onInitialize() {
		var holder = Config.holder();
		LOGGER.info( "File: {}", holder.getConfigFile() );
	}
}
