package testmod.config;

import blue.endless.jankson.Comment;
import com.enderzombi102.auraconfig.api.annotation.Config;
import com.enderzombi102.auraconfig.api.annotation.DescriptorModel;
import com.enderzombi102.auraconfig.api.annotation.Sync;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.NotNull;
import testmod.Const;

import java.util.List;
import java.util.Map;

import static com.enderzombi102.enderlib.collections.MapUtil.mapOf;
import static java.util.Map.entry;

@Sync( Sync.Mode.UseServer )
@Config( modid = Const.ID, filename = "testmod.json5", brigadier = true, modmenu = true, screen = true )
public class ConfigData {
	@Comment( "Additional (debug) logging switches" )
	public @NotNull LoggingData logging = new LoggingData();

	@Comment( "Feature switches, used to (dis|en)able entire features" )
	public @NotNull FeatureSwitchData features = new FeatureSwitchData();

	@Comment( "Spawn information" )
	@DescriptorModel.Tooltip( "Spawn information" )
	@DescriptorModel.Subgroup( "Spawn information" )
	public @NotNull SpawnData spawn = new SpawnData();

	@Comment( "Taxes configurations" )
	public @NotNull TaxesData taxes = new TaxesData();

	@Comment("""
	Categories of entities which are summonable by `/summon-random $category $amount`
	This is an object formatted as such: `{ $categoryName: [ $entityId, $entityId, ... ], ... }`
	""")
	public @NotNull Map<String, List<Identifier>> summonRandomCategories;
}
