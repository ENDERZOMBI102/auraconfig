package testmod.config;

import blue.endless.jankson.Comment;

public class TaxesData {
	@Comment( "The distance tax multiplier" )
	public long taxMultiplier = 100;
	@Comment( "The range within a claim costs" )
	public long taxZoneRange = 1000;
	@Comment( "The minimum tax fee" )
	public long minimumFee = 10;
	@Comment( "The maximum tax fee" )
	public long maximumFee = 1000000;
	@Comment("""
	The equation that controls the tax algorithm.
	Valid options are: `LINEAR`, `EXPONENTIAL_0`
	""")
	public EquationType equation = EquationType.EXPONENTIAL_0;
	@Comment( "Time in seconds that the input slot takes to switch to the next item, -1 to never switch" )
	public int claimScreenInputCycleTime = 5;

	public enum EquationType {
		LINEAR,
		EXPONENTIAL_0;
	}
}
