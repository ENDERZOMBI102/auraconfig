package testmod.config;

import blue.endless.jankson.Jankson;
import blue.endless.jankson.JsonGrammar;
import blue.endless.jankson.JsonPrimitive;
import blue.endless.jankson.api.Marshaller;
import com.enderzombi102.auraconfig.api.AuraConfig;
import com.enderzombi102.auraconfig.api.Holder;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.NotNull;

public abstract class Config {
	private static final @NotNull Holder<ConfigData> HOLDER = AuraConfig.register( ConfigData.class )
		.withJankson( Jankson.builder()
			.registerDeserializer( String.class, BlockPos.class, Config::readBlockPos )
			.registerSerializer( BlockPos.class, ( it, marshaller ) -> JsonPrimitive.of( it.toShortString() ) )
			.build() )
		.withGrammar( JsonGrammar.builder()
			.withComments( true )
			.printTrailingCommas( true )
			.bareSpecialNumerics( true )
			.printUnquotedKeys( true )
			.build() );

	private Config() { }

	public static @NotNull ConfigData get() {
		return HOLDER.get();
	}

	public static @NotNull Holder<ConfigData> holder() {
		return HOLDER;
	}

	private static @NotNull BlockPos readBlockPos( @NotNull String it, @NotNull Marshaller marshaller ) {
		var arr = it.split( ", " );
		assert arr.length == 3 : "Invalid BlockPos construct";
		return new BlockPos( Double.parseDouble( arr[ 0 ] ), Double.parseDouble( arr[ 1 ] ), Double.parseDouble( arr[ 2 ] ) );
	}
}
