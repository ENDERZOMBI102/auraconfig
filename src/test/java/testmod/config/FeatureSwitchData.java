package testmod.config;

import blue.endless.jankson.Comment;

public class FeatureSwitchData {
	@Comment( "Reload players origins when the server reload the datapacks" )
	public boolean reloadOrigins = true;

	@Comment( "Invasive modifications to goml's claim system to allow taxes ( Using new blocks is too easy :3 )" )
	public boolean claimModifications = false;

	@Comment( "DreamingCode's edits to goml, reimplemented as mixins" )
	public boolean dreamingGomlFixes = true;

	@Comment( "Hide name plates for everyone" )
	public boolean hideNamePlates = true;
}
