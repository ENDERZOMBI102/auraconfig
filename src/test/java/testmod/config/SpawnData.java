package testmod.config;

import blue.endless.jankson.Comment;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.Nullable;

public class SpawnData {
	@Comment( "The center block position of the spawn" )
	public @Nullable BlockPos centerPosition = null;

	@Comment( "The radius of the spawn, starting from the center, it is calculated as a cube" )
	public int radius = 0;
}
