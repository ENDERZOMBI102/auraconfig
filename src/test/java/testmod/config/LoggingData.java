package testmod.config;

import blue.endless.jankson.Comment;

public class LoggingData {
	@Comment( "Additional logging in `/summon-random`" )
	public boolean summonRandom = false;

	@Comment( "Additional logging for the claim anchor taxes mechanic" )
	public boolean claimAnchor = false;
	@Comment( "Additional logging for the claim anchor's screen" )
	public boolean claimAnchorScreen = false;

	@Comment( "Additional logging for the virtual resource pack" )
	public boolean virtualResourcePack = false;

}
